#!/bin/sh


DOMAIN_NAME="localhost"

# stop apache2
sudo apachectl stop
# stop mysql
sudo service mysql stop
sudo /etc/init.d/mysqld stop
# stop nginx
sudo nginx -s stop
sudo systemctl stop nginx


sudo service docker restart

sudo docker-compose up -d --build
sudo docker-compose start

sudo docker exec -i test200 /usr/bin/mysql -proot test200 -e "UPDATE products SET domain='${DOMAIN_NAME}'"
