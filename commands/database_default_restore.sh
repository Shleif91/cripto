#!/bin/sh


DOMAIN_NAME="ec2-52-90-135-66.compute-1.amazonaws.com"


docker exec -i test200 /usr/bin/mysql -proot -e "DROP DATABASE test200"
docker exec -i test200 /usr/bin/mysql -proot -e "CREATE DATABASE test200"
docker exec -i test200 /usr/bin/mysql -proot test200 < ../docker/mysql/dumps/admin_default.sql
docker exec -i test200 /usr/bin/mysql -proot test200 -e "UPDATE products SET domain='${DOMAIN_NAME}'"
