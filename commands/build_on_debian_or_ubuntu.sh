#!/bin/sh


DOMAIN_NAME="ec2-52-90-135-66.compute-1.amazonaws.com"


sudo service apache2 stop
sudo service mysql stop
sudo nginx -s stop

sudo service docker restart

sudo docker-compose up -d --build
sudo docker-compose start

sudo docker exec -i test200 /usr/bin/mysql -proot test200 -e "UPDATE products SET domain='${DOMAIN_NAME}'"
