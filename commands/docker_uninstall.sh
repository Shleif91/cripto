#!/bin/sh

# Ask for the user password
# Script only works if sudo caches the password for a few minutes
sudo true

sudo rm -rf /usr/local/bin/docker-compose
sudo apt-get purge docker-ce
sudo rm -rf /var/lib/docker
